from csv import writer, QUOTE_NONE
from random import uniform
from numpy import zeros
from sys import argv

n = int(argv[1])
a = zeros(shape=(n, n))

for i in range(0, n):
    for j in range(0, n):
        a[i, j] = round(uniform(0, n * 2), 2)

with open('num.csv', 'w') as f:
    w = writer(f, quoting=QUOTE_NONE)
    for k in range(0, n):
        w.writerow(a[k])

