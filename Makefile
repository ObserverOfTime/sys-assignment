CC = gcc
CFLAGS = -std=c99 -Wall -Wextra $(if $(DEBUG),-g3 -DDEBUG,-O3)
OBJS = $(addprefix $(BUILDDIR)/,memory.o threadpool.o)
LIBS = -pthread
BUILDDIR = build

## Compile both versions
all: MAKEFLAGS = -B --no-print-directory
all:
	@$(MAKE) main
	@$(MAKE) main-std

## Compile main with stdlib's malloc
main-std: CFLAGS += -DSTD_MALLOC
main-std: $(OBJS) src/main.c
	@mkdir -p $(BUILDDIR)
	$(CC) $^ $(CFLAGS) $(LIBS) -o $(BUILDDIR)/$@

## Compile main program
main: $(OBJS) src/main.c
	@mkdir -p $(BUILDDIR)
	$(CC) $^ $(CFLAGS) $(LIBS) -o $(BUILDDIR)/$@

## Compile object files
$(BUILDDIR)/%.o: src/%.c
	@mkdir -p $(BUILDDIR)
	$(CC) -c $< $(CFLAGS) $(LIBS) -o $@

## Remove build directory
clean: ; @rm -rfv $(BUILDDIR)

## Generate documentation with doxygen
docs: ; @doxygen

## Generate num.csv file
nums: ; @python gen_nums.py 2500

