# System Programming Assignment

## Summary

The threadpool utilises a [circular queue][queue]
to manage the tasks it needs to execute.
Each task consists of a function and its argument,
which can be of any type. When the threadpool is destroyed,
it waits for all the tasks to be completed before exiting

The memory manager uses 16 [free lists][list] with a step of 32KB.
It also has the same amount of mutexes, plus an extra one for `free`.
Each block of the free list consists of its size and a flag which defines
whether or not it is currently used. When no free blocks are available,
`sbrk` is used to provide ask the OS for extra memory.

The main program uses the threadpool to read through a
CSV file and sum up each of its lines.

Testing was done on Linux (specifically `Mint` & `Antergos`).

[queue]: https://en.wikipedia.org/wiki/Circular_buffer
[list]: https://en.wikipedia.org/wiki/Free_list

## Benchmarks

We ran some benchmarks using [hyperfine][hyperfine].<br>
(`#1` uses our `malloc`, `#2` uses the system's `malloc`)

```
$ hyperfine -r 10 -w 1 './build/main num.csv' './build/main-std num.csv'
Benchmark #1: ./build/main num.csv
  Time (mean ± σ):     149.6 ms ±  17.4 ms    [User: 147.5 ms, System: 89.3 ms]
  Range (min … max):   119.0 ms … 175.7 ms    10 runs

Benchmark #2: ./build/main-std num.csv
  Time (mean ± σ):     161.0 ms ±  12.7 ms    [User: 150.1 ms, System: 104.4 ms]
  Range (min … max):   143.6 ms … 178.5 ms    10 runs

Summary
  './build/main num.csv' ran
    1.08 ± 0.15 times faster than './build/main-std num.csv'
```

[hyperfine]: https://github.com/sharkdp/hyperfine

## Credits

* The threadpool was based on https://github.com/pminkov/threadpool.
* The memory manager was based on https://danluu.com/malloc-tutorial.

