#ifndef _MEMORY_H
#define _MEMORY_H

#include <stdbool.h>
#include <stddef.h>

/** The number of free lists to maintain. */
#define FREE_LIST_NUM 16

/**
 * @brief Calculates the size of a memory chunk.
 * @param i the index of a free list.
 */
#define CHUNK_SIZE(i) (size_t) (FREE_LIST_NUM * ((i) + 1) * 1024)

/** Memory block structure. */
struct block {
    struct block *next; /**< The next block in the list. */
    bool used; /**< Whether this block is used or not. */
    size_t size; /**< The size of this block. */
};

/** Memory block type. */
typedef struct block Block;

/**
 * @brief Custom `malloc` implementation.
 * @param bytes the number of bytes to allocate.
 * @return a pointer to the allocated bytes or `NULL`.
 */
void *xmalloc(size_t bytes);

/**
 * @brief Custom `free` implementation.
 * @param ptr the pointer to free.
 */
void xfree(void *ptr);

#ifndef STD_MALLOC
#define malloc(bytes) xmalloc(bytes)
#define free(ptr) xfree(ptr)
#endif /* ifndef STD_MALLOC */

#endif  /* ifndef _MEMORY_H */

