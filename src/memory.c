#define _DEFAULT_SOURCE

#include <assert.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

#include "logging.h"
#include "memory.h"

/** Like `NULL` except it's an error. */
#define ERROR ((void *) -1)

/**
 * @brief Initialises a mutex if it hasn't already been initialised.
 * @param mutex the mutex to initialise.
 */
#define MUTEX_INIT(mutex) \
    do { \
        if(!(mutex).initialised) { \
            pthread_mutex_init(&(mutex).lock, NULL); \
            (mutex).initialised = true; \
        } \
    } while(0)

/** Thread mutex structure. */
struct mutex {
    pthread_mutex_t lock; /**< The mutex lock. */
    bool initialised; /**< Initialisation flag. */
};

/** An array of mutexes. */
static struct mutex mutexes[FREE_LIST_NUM];

/** An extra mutex for @link xfree @endlink. */
static pthread_mutex_t free_mutex = PTHREAD_MUTEX_INITIALIZER;

/** An array of free lists. */
static Block *free_lists[FREE_LIST_NUM] = {NULL};

/**
 * @brief Allocates sufficient memory to a @link block @endlink.
 * @param bytes the number of bytes to allocate.
 * @return the allocated block.
 */
Block *alloc_block(size_t bytes) {
    Block *block = (Block *) sbrk(bytes + sizeof (Block));
    block->size  = bytes;
    block->next  = NULL;
    block->used  = true;
    return block;
}

/**
 * @brief Deallocates memory from a @link block @endlink.
 * @param block the block to deallocate.
 * @param index the index of the block's free list.
 */
void free_block(Block *block, int index) {
    assert(mutexes[index].initialised);
    pthread_mutex_lock(&mutexes[index].lock);

    if(free_lists[index] == NULL) {
        LOG("[FREE] The list is empty.");
        free_lists[index] = block;
    } else {
        LOG("[FREE] Pushing to the end of the list.");
        while(free_lists[index]->next != NULL)
            free_lists[index] = free_lists[index]->next;
        free_lists[index]->next = block;
    }

    pthread_mutex_unlock(&mutexes[index].lock);
}

void *xmalloc(size_t bytes) {
    // Do nothing on non-positive bytes
    if(bytes <= 0) return NULL;
    LOG("[ALLOC] Allocating %zu bytes.", bytes);

    // The block that will be returned
    Block *block;

    for(int i = 0; i < FREE_LIST_NUM - 1; ++i) {
        // Initialise mutexes
        MUTEX_INIT(mutexes[i]);

        // Allocate to the corresponding free list
        if(bytes <= CHUNK_SIZE(i)) {
            pthread_mutex_lock(&mutexes[i].lock);
            if(free_lists[i] == NULL) {
                LOG("[ALLOC] The list is empty.");
                block = alloc_block(CHUNK_SIZE(i));
            } else {
                block = free_lists[i];
                block->used = true;
                block->next = NULL;
                free_lists[i] = block->next;
            }
            pthread_mutex_unlock(&mutexes[i].lock);
            return (void *) (block + 1);
        }
    }

    // The last free list has no size limit
    LOG("[ALLOC] Using last free list.");
    MUTEX_INIT(mutexes[FREE_LIST_NUM - 1]);
    pthread_mutex_lock(&mutexes[FREE_LIST_NUM - 1].lock);

    if(free_lists[FREE_LIST_NUM - 1] == NULL) {
        LOG("[ALLOC] The last list is empty.");
        block = alloc_block(bytes);
    } else if(bytes <= free_lists[FREE_LIST_NUM - 1]->size) {
        LOG("[ALLOC] First block of the list is suitable.");
        block = free_lists[FREE_LIST_NUM - 1];
        block->used = true;
        free_lists[FREE_LIST_NUM - 1] = block->next;
        block->next = NULL;
    } else if(free_lists[FREE_LIST_NUM - 1]->next == NULL) {
        LOG("[ALLOC] Last block in the list.");
        block = alloc_block(bytes);
    } else {
        LOG("[ALLOC] Checking the middle blocks.");
        Block *head = free_lists[FREE_LIST_NUM - 1];
        Block *mid  = (head->next != NULL) ? head->next : NULL;
        Block *tail = (mid != NULL && mid->next != NULL) ? mid->next : NULL;

        for(block = NULL; head != NULL && mid != NULL; head = head->next) {
            if(bytes <= mid->size) {
                block = mid;
                head->next = mid->next;
                block->next = NULL;
                block->used = true;
                break;
            }
            mid = mid->next;
            if(tail != NULL) tail = tail->next;
        }

        if(block == NULL) {
            LOG("[ALLOC] Checking the last block.");
            if(mid != NULL && mid->size >= bytes) {
                block = mid;
                block->used = true;
                block->next = NULL;
            } else {
                LOG("[ALLOC] Allocating extra memory.");
                block = alloc_block(bytes);
            }
        }
    }

    pthread_mutex_unlock(&mutexes[FREE_LIST_NUM - 1].lock);
    assert(block != NULL && block != ERROR);
    return (void *) (block + 1);
}

void xfree(void *ptr) {
    Block *block = ((Block *) ptr) - 1;
    pthread_mutex_lock(&free_mutex);

    assert(block->used);
    block->used = false;
    block->next = NULL;

    for(int i = 0; i < FREE_LIST_NUM - 1; ++i) {
        if(block->size == CHUNK_SIZE(i)) {
            LOG("[FREE] Inside free list #%d.", i);
            assert(mutexes[i].initialised);
            pthread_mutex_unlock(&free_mutex);
            return free_block(block, i);
        }
    }

    pthread_mutex_unlock(&free_mutex);

    // xfree() might be called on the last free list before
    // xmalloc() so we try to initialise the mutex here too
    MUTEX_INIT(mutexes[FREE_LIST_NUM - 1]);
    LOG("[FREE] Inside last free list.");
    free_block(block, FREE_LIST_NUM - 1);
}

