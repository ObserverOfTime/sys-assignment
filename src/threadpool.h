#ifndef _THREADPOOL_H
#define _THREADPOOL_H

/** Shortcut for unsigned int. */
#define UINT unsigned int

/** Thread mutex type. */
typedef struct threadpool Threadpool;

/**
 * @brief Initialises a threadpool.
 * @param threads the number of threads in the pool.
 * @return the created threadpool, or `NULL` on error.
 */
Threadpool *threadpool_init(UINT threads);

/**
 * @brief Adds a new task in the queue of a threadpool.
 * @param pool the threadpool to which the task will be added.
 * @param func the function that will perform the task.
 * @param arg the argument to be passed to the function.
 */
void threadpool_add(Threadpool *pool, void *(*func)(void *), void *arg);

/**
 * @brief Blocks until the threadpool is done executing its tasks.
 * @param pool the threadpool to await.
 */
void threadpool_wait(Threadpool *pool);

/**
 * @brief Stops and destroys a threadpool.
 * @param pool the threadpool to destroy.
 */
void threadpool_destroy(Threadpool *pool);

#endif /* _THREADPOOL_H */

