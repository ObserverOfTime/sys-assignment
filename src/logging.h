#ifndef _LOGGING_H
#define _LOGGING_H

#ifdef DEBUG
#include <stdio.h>
/**
 * @brief Prints a message to stderr if `DEBUG` is defined.
 * @param msg the message to log.
 * @param ... extra log parameters.
 * @see https://pmihaylov.com/macros-in-c/
 */
#define LOG(msg, ...) \
    fprintf(stderr, "(%s:%d) " msg "\n", \
            __FILE__, __LINE__, ##__VA_ARGS__)
#else
#define LOG(...)
#endif /* ifndef DEBUG */

#endif /* ifndef _LOGGING_H */

