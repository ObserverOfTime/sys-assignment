#include <assert.h>
#include <pthread.h>
#include <unistd.h>

#ifdef STD_MALLOC
#include <stdlib.h>
#else
#include "memory.h"
#endif /* ifdef STD_MALLOC */

#include "logging.h"
#include "threadpool.h"

/** The maximum number of tasks of the pool. */
#define MAX_TASKS 1000

/** Thread mutex type. */
typedef struct task Task;

/** Thread task structure. */
struct task {
    void *(*func)(void *);
    void *arg;
};

/** Thread mutex structure. */
struct mutex {
    pthread_mutex_t lock; /**< The lock of the mutex. */
    pthread_cond_t available; /**< Signal for when there's available work. */
    pthread_cond_t done; /**< Signal for when all tasks are done. */
};

/** Queue structure. */
struct queue {
    Task *tasks; /**< The array of tasks in the queue. */
    UINT head; /**< The head of the queue. */
    UINT tail; /**< The tail of the queue. */
};

/** Thread pool structure. */
struct threadpool {
    pthread_t *threads; /**< The worker threads of the pool. */
    struct mutex mutex; /**< The mutex of the pool. */
    struct queue queue; /**< A circular queue of tasks to be executed. */
    UINT size; /**< The number of the threads in the pool. */
    UINT scheduled; /**< The number of scheduled tasks. */
};

/**
 * @brief Runs the threadpool's tasks.
 * @param pool_p `void` pointer to the threadpool.
 * @return `NULL`
 */
void *thread_run(void *pool_p) {
    LOG("[WORKER] Starting work thread.");
    Threadpool *pool = (Threadpool *) pool_p;
    for(;;) {
        pthread_mutex_lock(&pool->mutex.lock);

        while(pool->queue.head == pool->queue.tail) {
            LOG("[WORKER] Empty queue. Waiting...");
            pthread_cond_wait(&pool->mutex.available, &pool->mutex.lock);
        }

        assert(pool->queue.head != pool->queue.tail);
        LOG("[WORKER] Picked %d", pool->queue.head);
        Task task = pool->queue.tasks[(pool->queue.head)++ % MAX_TASKS];

        // The task is scheduled.
        ++(pool->scheduled);

        pthread_mutex_unlock(&pool->mutex.lock);

        // Run the task.
        task.func(task.arg);

        pthread_mutex_lock(&pool->mutex.lock);

        if(--(pool->scheduled) == 0) pthread_cond_signal(&pool->mutex.done);
        pthread_mutex_unlock(&pool->mutex.lock);
    }
    return NULL;
}

void threadpool_add(Threadpool *pool, void *(*func)(void *), void *arg) {
    pthread_mutex_lock(&pool->mutex.lock);
    LOG("[QUEUE] Queueing one item.");
    if(pool->queue.head == pool->queue.tail)
        pthread_cond_broadcast(&pool->mutex.available);

    Task task = {func, arg};
    pool->queue.tasks[(pool->queue.tail)++ % MAX_TASKS] = task;

    pthread_mutex_unlock(&pool->mutex.lock);
}

void threadpool_wait(Threadpool *pool) {
    LOG("[POOL] Waiting for completion.");
    pthread_mutex_lock(&pool->mutex.lock);
    while(pool->scheduled > 0)
        pthread_cond_wait(&pool->mutex.done, &pool->mutex.lock);
    pthread_mutex_unlock(&pool->mutex.lock);
    LOG("[POOL] Waiting done.");
}

Threadpool *threadpool_init(UINT threads) {
    Threadpool *pool = malloc(sizeof(Threadpool));

    pool->scheduled = 0;
    pool->queue.tasks = malloc(sizeof(Task) * MAX_TASKS);
    pool->queue.head = pool->queue.tail = 0;

    pool->size = threads;
    pool->threads = malloc(sizeof(pthread_t) * threads);

    pthread_mutex_init(&pool->mutex.lock, NULL);
    pthread_cond_init(&pool->mutex.available, NULL);
    pthread_cond_init(&pool->mutex.done, NULL);

    for(UINT i = 0; i < threads; ++i)
        pthread_create(&pool->threads[i], NULL, thread_run, pool);

    return pool;
}

void threadpool_destroy(Threadpool *pool) {
    threadpool_wait(pool);

    for(UINT i = 0; i < pool->size; ++i)
        pthread_detach(pool->threads[i]);

    free(pool->threads);
    free(pool->queue.tasks);
    free(pool);
}

