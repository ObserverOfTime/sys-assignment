#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef STD_MALLOC
#include "memory.h"
#endif /* ifndef STD_MALLOC */

#include "threadpool.h"

/**
 * @brief Sums up a line and prints the result.
 * @param arg a pointer to the line string.
 */
void *sum_line(void *arg) {
    char *line = (char *) arg;
    char *tok = strtok(line, ",");
    float sum = 0;
    while(tok != NULL) {
        sum += strtof(tok, NULL);
        tok = strtok(NULL, ",");
    }
    printf("Sum is: %.2f\n", sum);
    return NULL;
}

int main(int argc, char **argv) {
    if(argc < 2) {
        fprintf(stderr, "Please provide a file.\n");
        return 1;
    };
    // Open the provided file
    FILE *fp = fopen(argv[1], "r");

    // Initialise the threadpool
    Threadpool *pool = threadpool_init(5);

    size_t len = 0;
    while(!feof(fp)) {
        // Read line by line
        char *line = NULL;
        getline(&line, &len, fp);
        threadpool_add(pool, sum_line, (void *) line);
    }

    threadpool_destroy(pool);
    return 0;
}

